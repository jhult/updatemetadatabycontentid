﻿# UpdateMetadataByContentID

## Component Information
* Author: Jonathan Hult
* Last Updated: build_1_20141114
* License: MIT

## Overview
This WebCenter Content component adds a service UPDATE_DOCINFO_BY_DOC_NAME which first calls QlatestReleasedIDByName query to retrieve the latest dID and then calls the SubService UPDATE_DOCINFO_SUB. This allows updating metadata by using dDocName.

* Services:
	- UPDATE_DOCINFO_BY_DOC_NAME: Custom - Calls QlatestReleasedIDByName query and then subservice UPDATE_DOCINFO_SUB; Required parameter: dDocName

## Compatibility
This component was built upon and tested on the version listed below, this is the only version it is known to work on, but it may work on older/newer versions.

* 10.1.3.5.1 (111229) (Build:7.2.4.105)

## Changelog
* build_1_20141114
	- Initial component release